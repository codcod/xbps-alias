# An XBPS helper alias

Script that exposes [XBPS](http://docs.voidlinux.org/xbps) commands via
a bit friendlier interface.

Download it:

    git clone https://codeberg/codcod/xbps-alias.git ~/.config/xbps-alias

and source it by adding the following line to `.zshrc`:

    [ -f ~/.config/xbps-alias/xbps-alias ] && source ~/.config/xbps-alias/xbps-alias

## Usage

Available options:

- `add`: install a package (aliases: `install`)
- `rm`: uninstall a package (aliases: `remove`, `uninstall`)
- `find`: search for a package (aliases: `query`, `search`)
- `update`: update all packages

## Caveat

Tested with ZSH only.

## References

Similar projects:
- [VPM](https://github.com/netzverweigerer/vpm)

